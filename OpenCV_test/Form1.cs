﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.XImgproc;
using System.Diagnostics;

namespace OpenCV_test
{
    public partial class Form1 : Form
    {
        Size old = new Size();
        Stopwatch sw = new Stopwatch();
        TimeSpan ts = new TimeSpan();

        static public int cannyThresh1 = 10;
        static public int cannyThresh2 = 30;
        static public bool cannyL2Gradient = false;
        static public int cannyAperatureSize = 3;
        static public int sobelKSize = 3;
        static public double sobelScale = 1;
        static public double sobelDelta = 0;

        Image<Bgr, Byte> lastImage;

        string lastfile = "";

        public Form1()
        {
            InitializeComponent();
            old.Height = this.Height;
            old.Width = this.Width;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //lastfile = openFileDialog1.FileName;
                lastImage = new Image<Bgr, byte>(openFileDialog1.FileName);

                //Compute(openFileDialog1.FileName);
                Compute(lastImage);
                buttonCompute.Enabled = true;
            }
        }

        private void Compute(Image<Bgr, Byte> MyImage)
        {
            sw.Start();

            //Image<Bgr, Byte> MyImage = new Image<Bgr, byte>(filename);
            //pictureBox1.Image = MyImage.ToBitmap();

            /*
            ts = sw.Elapsed;
            label1.Text = "Loaded image - " + ts.TotalMinutes.ToString("#.#####") + " s";
            */
            sw.Restart();

            /// Convert the image to grayscale
            UMat grayimage = new UMat();
            CvInvoke.CvtColor(MyImage, grayimage, ColorConversion.Bgr2Gray);

            /// Show grayscale image
            MyImage = grayimage.ToImage<Bgr, byte>();
            pictureBox1.Image = MyImage.ToBitmap();

            ts = sw.Elapsed;
            label1.Text = "Grayscale- " + ts.TotalMinutes.ToString("#.#####") + " s";
            sw.Restart();

            //use image pyr to remove noise
            UMat pyrDown = new UMat();
            CvInvoke.PyrDown(grayimage, pyrDown);
            CvInvoke.PyrUp(pyrDown, grayimage);

            UMat cannyEdges = new UMat();
            CvInvoke.Canny(grayimage, cannyEdges, cannyThresh1, cannyThresh2, cannyAperatureSize, cannyL2Gradient);

            // Show canny contours
            MyImage = cannyEdges.ToImage<Bgr, byte>();
            pictureBox2.Image = MyImage.ToBitmap();

            ts = sw.Elapsed;
            label2.Text = "Canny- " + ts.TotalMinutes.ToString("#.#####") + " s";
            sw.Restart();

            UMat pyrDown2 = new UMat();
            CvInvoke.PyrDown(grayimage, pyrDown2);
            CvInvoke.PyrUp(pyrDown2, grayimage);

            //SobelX
            UMat sobelX = new UMat();
            CvInvoke.Sobel(grayimage, sobelX, grayimage.Depth, 1, 0, sobelKSize, sobelScale, sobelDelta, BorderType.Replicate);

            UMat sobelY = new UMat();
            CvInvoke.Sobel(grayimage, sobelY, grayimage.Depth, 0, 1, sobelKSize, sobelScale, sobelDelta, BorderType.Replicate);

            //Combine sobel X and Y            
            UMat absSobelX = new UMat();
            UMat absSobelY = new UMat();

            UMat CombinedSobel = new UMat();
            CvInvoke.ConvertScaleAbs(sobelX, absSobelX, 1, 0);
            CvInvoke.ConvertScaleAbs(sobelY, absSobelY, 1, 0);
            /*
            CvInvoke.AddWeighted(absSobelX, 0.5, absSobelY, 0.5, 0, CombinedSobel);

            CvInvoke.Threshold(CombinedSobel, CombinedSobel, 0, 255, ThresholdType.Binary | ThresholdType.Otsu);

            XImgprocInvoke.Thinning(CombinedSobel, CombinedSobel, ThinningTypes.GuoHall);

            ts = sw.Elapsed;
            label5.Text = "Sobel AddWeighted- " + ts.TotalMinutes.ToString("#.#####") + " s";
            sw.Restart();
            
            MyImage = CombinedSobel.ToImage<Bgr, byte>();
            pictureBox5.Image = MyImage.ToBitmap();
            */
            // Sqrt(sobelX^2+sobelY^2)
            UMat xSobel2 = new UMat();
            UMat ySobel2 = new UMat();
            UMat ComSobel2 = new UMat();
            UMat ComSobel2F = new UMat();

            UMat xSobel2Tmp = new UMat();
            UMat ySobel2Tmp = new UMat();
            xSobel2.ConvertTo(xSobel2Tmp, DepthType.Cv32F);
            ySobel2.ConvertTo(ySobel2Tmp, DepthType.Cv32F);

            CvInvoke.Pow(xSobel2Tmp, 2, xSobel2);
            CvInvoke.Pow(ySobel2Tmp, 2, ySobel2);
            CvInvoke.Add(absSobelX, absSobelY, ComSobel2);

            UMat Tmp = new UMat();
            ComSobel2.ConvertTo(Tmp, DepthType.Cv32F);

            CvInvoke.Sqrt(Tmp, ComSobel2F);

            MyImage = ComSobel2F.ToImage<Bgr, byte>();
            pictureBox3.Image = MyImage.ToBitmap();

            ts = sw.Elapsed;
            label3.Text = "Sobel before thresh- " + ts.TotalMinutes.ToString("#.#####") + " s";
            sw.Restart();

            ComSobel2F.ConvertTo(ComSobel2F, DepthType.Cv8U);
            CvInvoke.Threshold(ComSobel2F, ComSobel2F, 0, 255, ThresholdType.Binary | ThresholdType.Otsu);

            XImgprocInvoke.Thinning(ComSobel2F, ComSobel2F, ThinningTypes.GuoHall);

            MyImage = ComSobel2F.ToImage<Bgr, byte>();
            pictureBox4.Image = MyImage.ToBitmap();

            ts = sw.Elapsed;
            label4.Text = "Sobel after thinning- " + ts.TotalMinutes.ToString("#.#####") + " s";
            sw.Stop();
            //float[,] HH = new float[,] { { 0.06f, -0.6f, 0.5f }, { 0.12f, -1.2f, 1f }, { 0.06f, -0.6f, 0.5f } };
            //float[,] HV = new float[,] { { 0.06f, 0.12f, 0.06f }, { -0.6f, -1.2f, -0.6f }, { 0.5f, 1f, 0.5f } };
            UMat gh2 = new UMat(grayimage.Size, DepthType.Cv32F, 1);
            UMat gv2 = new UMat(grayimage.Size, DepthType.Cv32F, 1);
            UMat zeros = new UMat(grayimage.Size, DepthType.Cv32F, 1);
            grayimage.ConvertTo(grayimage, DepthType.Cv32F);
            //gh2.ConvertTo(gh2, DepthType.Cv32F);
            //gv2.ConvertTo(gv2, DepthType.Cv32F);

            CvInvoke.Filter2D(grayimage, gh2, HH, new Point(-1, -1), 0, BorderType.Replicate);
            CvInvoke.Filter2D(grayimage, gv2, HV, new Point(-1, -1), 0, BorderType.Replicate);
            CvInvoke.Filter2D(grayimage, zeros, ZE, new Point(-1, -1), 0, BorderType.Replicate);
            CvInvoke.Max(gh2, zeros, gh2);
            CvInvoke.Max(gv2, zeros, gv2);
            UMat gh2Tmp = new UMat();
            UMat gv2Tmp = new UMat();
            gh2.ConvertTo(gh2Tmp, DepthType.Cv32F);
            gv2.ConvertTo(gv2Tmp, DepthType.Cv32F);

            CvInvoke.Pow(gh2Tmp, 2, gh2);
            CvInvoke.Pow(gv2Tmp, 2, gv2);
            UMat combinedg2 = new UMat();
            CvInvoke.Add(gh2, gv2, combinedg2);

            UMat Tmp2 = new UMat();
            combinedg2.ConvertTo(Tmp2, DepthType.Cv32F);

            CvInvoke.Sqrt(Tmp2, combinedg2);

            MyImage = combinedg2.ToImage<Bgr, byte>();
            pictureBox5.Image = MyImage.ToBitmap();

            ts = sw.Elapsed;
            label5.Text = "Fract before thresh- " + ts.TotalMinutes.ToString("#.#####") + " s";
            sw.Restart();

            combinedg2.ConvertTo(combinedg2, DepthType.Cv8U);
            CvInvoke.Threshold(combinedg2, combinedg2, 0, 255, ThresholdType.Binary | ThresholdType.Otsu);

            XImgprocInvoke.Thinning(combinedg2, combinedg2, ThinningTypes.GuoHall);

            MyImage = combinedg2.ToImage<Bgr, byte>();
            pictureBox6.Image = MyImage.ToBitmap();

            ts = sw.Elapsed;
            label6.Text = "Fract after thinning- " + ts.TotalMinutes.ToString("#.#####") + " s";
            sw.Stop();


        }

        Matrix<float> HH = new Matrix<float>(
            new float[,] {
                {0.06f, -0.6f, 0.5f},
                {0.12f, -1.2f, 1.0f},
                {0.06f, -0.6f, 0.5f}
            }
        );

        Matrix<float> HV = new Matrix<float>(
            new float[,] {
                {0.06f, 0.12f, 0.06f},
                {-0.6f, -1.2f, -0.6f},
                {0.5f , 1.0f , 0.5f}
            }
        );

        Matrix<float> ZE = new Matrix<float>(
            new float[,] {
                {0.0f, 0.0f, 0.0f},
                {0.0f, 0.0f, 0.0f},
                {0.0f ,0.0f ,0.0f}
            }
        );

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            this.pictureBox1.Location = new Point(5,                        5);
            this.pictureBox1.Size = new Size(    ((this.Width - 20) / 3)-25,         ((this.Height)-205)/2);

            this.pictureBox2.Location = new Point(((this.Width-20) / 3) + 10,     5);
            this.pictureBox2.Size = new Size(     ((this.Width - 20) / 3) - 25,     ((this.Height) - 205) / 2);

            this.pictureBox3.Location = new Point(((this.Width - 20) / 3) * 2 + 15, 5);
            this.pictureBox3.Size = new Size(     ((this.Width - 20) / 3) - 25,     ((this.Height) - 205) / 2);

            this.pictureBox4.Location = new Point(5,                        ((this.Height) - 205) / 2 + 50);
            this.pictureBox4.Size = new Size(    ((this.Width - 20) / 3) - 25,     ((this.Height) - 205) / 2);

            this.pictureBox5.Location = new Point(((this.Width - 20) / 3) + 10,     ((this.Height) - 205) / 2 + 50);
            this.pictureBox5.Size = new Size(     ((this.Width - 20) / 3) - 25,     ((this.Height) - 205) / 2);

            this.pictureBox6.Location = new Point(((this.Width - 20) / 3) * 2 + 15, ((this.Height) - 205) / 2 + 50);
            this.pictureBox6.Size = new Size(     ((this.Width - 20) / 3) - 25,     ((this.Height) - 205) / 2);

            this.label1.Location = new Point(30, ((this.Height) - 190) / 2);
            this.label2.Location = new Point(((this.Width - 20) / 3) + 50, ((this.Height) - 190) / 2);
            this.label3.Location = new Point(((this.Width - 20) / 3) * 2 + 50, ((this.Height) - 190) / 2);
            this.label4.Location = new Point(30, ((this.Height) - 150));
            this.label5.Location = new Point(((this.Width - 20) / 3) + 50, ((this.Height) - 150));
            this.label6.Location = new Point(((this.Width - 20) / 3) * 2 + 50, ((this.Height) - 150));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //buttonCompute.Text = "Working";
            Compute(lastImage);
            //buttonCompute.Text = "Compute";
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            Settings setform = new Settings();
            setform.Show();
        }
    }
}
