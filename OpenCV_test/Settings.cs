﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OpenCV_test
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void hScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            Form1.cannyThresh1 = hScrollBar1.Value;
            labelCannyTh1.Text = "Threshold lower: " + hScrollBar1.Value.ToString();
        }

        private void hScrollBar2_ValueChanged(object sender, EventArgs e)
        {
            Form1.cannyThresh2 = hScrollBar2.Value;
            labelCannyTh2.Text = "Threshold upper: " + hScrollBar2.Value.ToString();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Form1.cannyL2Gradient = checkBox1.Checked;
        }
        
        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            int aparature = trackBar1.Value * 2 + 1;
            Form1.cannyAperatureSize = aparature;
            labelCannyAparat.Text = "Aperature size: " + aparature.ToString(); 
        }

        private void trackBar2_ValueChanged(object sender, EventArgs e)
        {
            Form1.sobelKSize = trackBar2.Value * 2 + 1;
            label3.Text = "kSize: " + (trackBar2.Value * 2 + 1).ToString();
        }

        private void hScrollBar3_ValueChanged(object sender, EventArgs e)
        {
            Form1.sobelScale = Convert.ToDouble(hScrollBar3.Value) / 10;
            label1.Text = "Scale: " + (Convert.ToDouble(hScrollBar3.Value) / 10).ToString();
        }

        private void hScrollBar4_ValueChanged(object sender, EventArgs e)
        {
            Form1.sobelDelta = Convert.ToDouble(hScrollBar4.Value) / 10;
            label2.Text = "Delta: " + (Convert.ToDouble(hScrollBar4.Value) / 10).ToString();
        }
    }
}
